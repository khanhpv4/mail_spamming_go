package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"text/template"

	"gitlab.com/khanhpv4/mail_spamming_go/job"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	mailutils "gitlab.com/khanhpv4/mail_spamming_go/mail-utils"
)

const (
	uploadDir = "./uploads/"
)

// TemplateRenderer is a custom html/template renderer for Echo framework
type TemplateRenderer struct {
	templates *template.Template
}

// Render renders a template document
func (t *TemplateRenderer) Render(w io.Writer, name string, data interface{}, c echo.Context) error {

	// Add global methods if data is a map
	if viewContext, isMap := data.(map[string]interface{}); isMap {
		viewContext["reverse"] = c.Echo().Reverse
	}

	return t.templates.ExecuteTemplate(w, name, data)
}
func main() {
	e := echo.New()
	renderer := &TemplateRenderer{
		templates: template.Must(template.ParseGlob("./*.html")),
	}
	e.Renderer = renderer
	// e.Use(middleware.Logger())
	e.Use(middleware.Gzip())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{ // Set header config
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAuthorization},
		AllowMethods: []string{"GET", "POST", "OPTIONS", "PUT", "DELETE"},
	}))
	e.Static("/css", "css")
	e.Static("/js", "js")
	e.Static("/fonts", "fonts")
	e.Static("/images", "images")
	e.Static("/vendor", "vendor")
	e.GET("/", func(c echo.Context) error {
		return c.Render(http.StatusOK, "template.html", map[string]interface{}{})
	}).Name = "index"
	e.GET("/jobdetail", func(c echo.Context) error {
		return c.Render(http.StatusOK, "job.html", map[string]interface{}{})
	}).Name = "job"
	e.POST("/", sendMailHandler)
	e.GET("/job/:id", func(c echo.Context) error {
		return c.JSON(http.StatusOK, job.GetJob(c.Param("id")))
	})
	e.Logger.Fatal(e.Start(":80"))
}

func sendMailHandler(c echo.Context) error {
	//textarea tag, name="senders", input format smtp_host||smtp_port||email||password
	//for multiple smtp:
	/*
		smtp_host1||smtp_port1||email1||password1
		smtp_host2||smtp_port2||email2||password2
		smtp_host3||smtp_port3||email3||password3
	*/
	senderMails := c.FormValue("senders")
	//textarea tag, name="recipients", input format email||name||surname
	/*
		email1||name1||surname1
		email2||name2||surname2
		email3||name3||surname3
	*/
	recipientMails := c.FormValue("recipients")
	//textarea or input tag, name="subjects", input format subject1||subject2||subject3||subject4
	//suject will be picked at random from set {subject1, subject2, subject3, subject4}
	subjects := c.FormValue("subjects")
	//textarea tag, name="content", input format HTML (best)
	content := c.FormValue("content")
	//input tag
	reply := c.FormValue("reply")
	var attachments []string

	// Attachments multiple form
	form, err := c.MultipartForm()
	if err != nil {
		return c.HTML(http.StatusInternalServerError, fmt.Sprintf("<p>Error %s</p>", err.Error()))
	}
	//input tag, multiple file selection
	files := form.File["fileToUpload"]

	for _, file := range files {
		// Source
		src, err := file.Open()
		if err != nil {
			return c.HTML(http.StatusInternalServerError, fmt.Sprintf("<p>Error %s</p>", err.Error()))
		}
		defer src.Close()

		// Destination
		filename := uploadDir + file.Filename
		dst, err := os.Create(filename)
		if err != nil {
			return err
		}
		defer dst.Close()

		// Copy
		if _, err = io.Copy(dst, src); err != nil {
			return c.HTML(http.StatusInternalServerError, fmt.Sprintf("<p>Error %s</p>", err.Error()))
		}
		attachments = append(attachments, filename)
	}
	j := job.Job{}
	id, err := job.AddNewJob(j)
	if err != nil {
		return c.HTML(http.StatusInternalServerError, fmt.Sprintf("<p>Error %s</p>", err.Error()))
	}
	err = mailutils.SendBulkMail(id, senderMails, recipientMails, subjects, content, reply, attachments)
	if err != nil {
		return c.HTML(http.StatusInternalServerError, fmt.Sprintf("<p>Error %s</p>", err.Error()))
	}
	return c.Redirect(http.StatusFound, "/jobdetail?id="+id)
}
