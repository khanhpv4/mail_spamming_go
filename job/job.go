package job

import (
	"crypto/md5"
	"fmt"
	"io"
	"sync"
	"time"
)

type Job struct {
	Total        int
	Passes       int
	Fails        int
	ErrorDetails []ErrorDetail
}

type ErrorDetail struct {
	From  string
	To    string
	Error string
}

var Jobpool = map[string]Job{}
var JobMutex = &sync.Mutex{}

func AddNewJob(j Job) (string, error) {
	id, err := getmd5Time()
	if err != nil {
		return "", err
	}
	JobMutex.Lock()
	Jobpool[id] = j
	JobMutex.Unlock()
	return id, nil
}

func UpdateJob(id string, j Job) {
	JobMutex.Lock()
	Jobpool[id] = j
	JobMutex.Unlock()
}

func GetJob(id string) Job {
	return Jobpool[id]
}

func RemoveJob(id string) {
	JobMutex.Lock()
	delete(Jobpool, id)
	JobMutex.Unlock()
}

func getmd5Time() (string, error) {
	h := md5.New()
	t := time.Now()
	_, err := io.WriteString(h, t.String())
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%x", h.Sum(nil)), nil
}
