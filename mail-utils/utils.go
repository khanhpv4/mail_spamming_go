package mailutils

import (
	"errors"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

type Recipient struct {
	Email   string
	Name    string
	Surname string
	// Reply   string
}

func StringToSenderSlice(sendersStr string) (res []Sender, err error) {
	senders := strings.Split(sendersStr, "\r\n")
	if len(senders) < 1 {
		err = errors.New("missing senders")
		return
	}
	for _, v := range senders {
		info := strings.Split(v, "||")
		if len(info) != 5 {
			err = errors.New("wrong format")
			return
		}
		i, err := strconv.Atoi(info[1])
		if err != nil {
			return nil, err
		}
		res = append(res, Sender{
			// SmtpHost: info[0],
			// SmtpPort: i,
			Email: info[2],
			Name:  info[4],
			// Password: info[3],
			Dialer: NewSender(info[0], i, info[2], info[3]),
		})
	}
	return
}

func StringToRecipientSlice(recipientsStr string) (res []Recipient, err error) {
	recipients := strings.Split(recipientsStr, "\r\n")
	if len(recipients) < 1 {
		err = errors.New("missing recipients")
		return
	}
	for _, v := range recipients {
		info := strings.Split(v, "||")
		if len(info) != 3 {
			err = errors.New("wrong format")
			return
		}
		res = append(res, Recipient{
			Email:   info[0],
			Name:    info[1],
			Surname: info[2],
			// Reply:   info[3],
		})
	}
	return
}

func StringToSubjectSlice(subjectsStr string) (res []string, err error) {
	res = strings.Split(subjectsStr, "||")
	if len(res) < 1 {
		err = errors.New("missing recipients")
		return
	}
	return
}

func RandInt(rangeInt int) int {
	rand.Seed(time.Now().Unix())
	return rand.Intn(rangeInt)
}
