package mailutils

import (
	"log"
	"time"

	"gitlab.com/khanhpv4/mail_spamming_go/job"
)

func SendBulkMail(jobID, sendersStr, recipientsStr, subjectsStr, content, reply string, attachments []string) error {
	senders, err := StringToSenderSlice(sendersStr)
	if err != nil {
		return err
	}
	recipients, err := StringToRecipientSlice(recipientsStr)
	if err != nil {
		return err
	}
	subjects, err := StringToSubjectSlice(subjectsStr)
	if err != nil {
		return err
	}
	job.UpdateJob(jobID, job.Job{
		Total: len(recipients),
	})
	recipientChan := make(chan Recipient, len(recipients))
	for _, r := range recipients {
		recipientChan <- r
	}
	close(recipientChan)

	for _, sender := range senders {
		go sendMailWorker(sender, recipientChan, jobID, reply, content, attachments, subjects)
	}
	return nil
}

func sendMailWorker(sender Sender, recipientChan chan Recipient, jobID, reply, content string, attachments, subjects []string) {
	for r := range recipientChan {
		currentJob := job.GetJob(jobID)
		subject := subjects[RandInt(len(subjects))]
		err := sender.SendMailWorker(r, reply, content, subject, attachments)
		if err != nil {
			log.Printf("FAILED! Could NOT send mail '%s' FROM '%s' TO '%s'. Error: %s", subject, sender.Email, r.Email, err.Error())
			job.UpdateJob(jobID, job.Job{
				Total:  currentJob.Total,
				Passes: currentJob.Passes,
				Fails:  currentJob.Fails + 1,
				ErrorDetails: append(currentJob.ErrorDetails, job.ErrorDetail{
					From:  sender.Email,
					To:    r.Email,
					Error: err.Error(),
				}),
			})
			continue
		}
		log.Printf("SENT! Mail '%s' has been sent FROM '%s' TO '%s'\n", subject, sender.Email, r.Email)
		job.UpdateJob(jobID, job.Job{
			Total:        currentJob.Total,
			Passes:       currentJob.Passes + 1,
			Fails:        currentJob.Fails,
			ErrorDetails: currentJob.ErrorDetails,
		})
	}
	time.Sleep(24 * time.Hour) //Job status will be remove after 24 hours
	job.RemoveJob(jobID)
}
