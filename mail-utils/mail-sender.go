package mailutils

import (
	"strings"

	gomail "gopkg.in/gomail.v2"
)

type Sender struct {
	// SmtpHost string
	// SmtpPort int
	Email string
	Name  string
	// Password string
	Dialer *gomail.Dialer
}

func NewSender(smtpHost string, smtpPort int, email, password string) *gomail.Dialer {
	return gomail.NewPlainDialer(smtpHost, smtpPort, email, password)
}

func (s *Sender) SendMailWorker(recipient Recipient, reply, content, subject string, attachments []string) error {
	content = strings.Replace(content, "{{name}}", recipient.Name, -1)
	content = strings.Replace(content, "{{surname}}", recipient.Surname, -1)
	content = strings.Replace(content, "{{sendername}}", s.Name, -1)
	m := gomail.NewMessage()
	m.SetHeader("From", s.Email)
	m.SetHeader("To", recipient.Email)
	m.SetHeader("Subject", subject)
	m.SetHeader("Reply-To", reply)
	m.SetBody("text/html", content)
	for _, v := range attachments {
		m.Attach(v)
	}
	// Send the email
	return s.Dialer.DialAndSend(m)
}
